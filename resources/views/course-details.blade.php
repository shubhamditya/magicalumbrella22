@extends('layouts.base')

@section('title')
    {{ $course->title }}
@endsection

@section('content')
    <section class="container-fluid " id="course-hero" x-data="{ open: false }">
        <div class="row max_width flex_reverse ">
            <div class="col-sm-6 mobile_margin_top desktop_flex_center">
                <h1>{{ $course->title }}</h1>
                <p>{{ $course->excerpt }}</p>
                @if (!$user_brought)
                    <div class="price mobile_hide">
                        <h2>₹{{ $course->price / 100 }}</h2>
                        <span>₹{{ $course->discount_price / 100 }}</span>
                    </div>
                    <div class="buy_btn mobile_hide">
                        <a href="{{ route('razorpay.index', $course->slug) }}">get course</a>
                    </div>
                @else
                    <div class="price" style="display: flex;flex-direction: column;">
                        @if (session('status'))
                            <h2 style="font-size: 20px" class="text-success">
                                {{ session('status') }}
                            </h2>
                        @endif
                        @if ($batch_bool)
                            <h2 style="font-size: 20px">Batch Start In {{ $course->batch_start_at->format('d M Y') }}</h2>
                        @endif
                        <h2 style="font-size: 20px">You will Be Notified By Mail and SMS</h2>
                    </div>
                @endif
            </div>
            <div class="col-sm-6 desktop_flex_center">
                <div class="image">
                    <img src="{{ asset('storage/' . $course->thumbnail) }}" alt="{{ $course->title }}" height="300">
                    <button class="play_btn" @click="open = true">
                        <img src="https://img.icons8.com/ios-glyphs/90/000000/play--v1.png" />
                    </button>
                </div>
            </div>
        </div>
        <div id="video_overlay" x-bind:class="open ? 'active' : ''">
            <div class="video_container">
                <div class="close" @click="open = false">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 172 172"
                        style=" fill:#000000;">
                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                            stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                            font-family="none" font-weight="none" font-size="none" text-anchor="none"
                            style="mix-blend-mode: normal">
                            <path d="M0,172v-172h172v172z" fill="none"></path>
                            <g fill="#ffffff">
                                <path
                                    d="M40.13333,22.93333c-1.46702,0 -2.93565,0.55882 -4.05365,1.67969l-11.46667,11.46667c-2.24173,2.24173 -2.24173,5.87129 0,8.10729l41.81302,41.81302l-41.81302,41.81302c-2.24173,2.24173 -2.24173,5.87129 0,8.10729l11.46667,11.46667c2.24173,2.24173 5.87129,2.24173 8.10729,0l41.81302,-41.81302l41.81302,41.81302c2.236,2.24173 5.87129,2.24173 8.10729,0l11.46667,-11.46667c2.24173,-2.24173 2.24173,-5.87129 0,-8.10729l-41.81302,-41.81302l41.81302,-41.81302c2.24173,-2.236 2.24173,-5.87129 0,-8.10729l-11.46667,-11.46667c-2.24173,-2.24173 -5.87129,-2.24173 -8.10729,0l-41.81302,41.81302l-41.81302,-41.81302c-1.12087,-1.12087 -2.58663,-1.67969 -4.05365,-1.67969z">
                                </path>
                            </g>
                        </g>
                    </svg>
                </div>
                <video src="{{ asset('storage/' . $course->demo_video) }}" controls></video>
            </div>
        </div>
    </section>
    @if (!$user_brought)
        <section id="buynow" class="desktop_hide">
            <div class="price">
                <h2>₹{{ $course->price / 100 }}</h2>
            </div>
            <div class="buy_btn">
                <a href="{{ route('razorpay.index', $course->slug) }}">get course</a>
            </div>
        </section>
    @endif
    <section class="container-fluid max_width" id="description">
        <div class="row">
            <div class="col-md-7">
                <div class="course_description">

                    <h3>courses description</h3>

                    <p> {{ $course->description }}</p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="course_description">

                    <h3>services</h3>

                    <ul>
                        @foreach ($course->services as $service)
                            <li>{{ $service->services }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('app.js') }}"></script>
@endsection
