@extends('layouts.base')

@section('title')
    Checkout Page
@endsection

@section('content')

    <section class="container-fluid" id="checkout">
        <div class="row">
            <div class="col-md-12 checkout-wrapper">
                @if (session('error'))
                    <small>
                        {{ session('error') }}
                    </small>
                @endif
                <div class="checkout-container">
                    <img src="{{ asset('storage/' . $course->thumbnail) }}" alt="{{ $course->title }}">
                    <div class="checkout_card shadow">
                        <h1>{{ $course->title }}</h1>
                        <div class="d-flex mb-3"
                            style="justify-content: flex-end;flex-direction: column;align-items: flex-end">
                            <h2>₹{{ $course->price / 100 }}</h2>
                            <h2>+ ₹{{ $gst_price_only / 100 }}</h2>
                            <h2><span class="text-secondary">(+18%
                                    GST)</span>&nbsp;&nbsp;₹{{ $gst_price / 100 }}
                            </h2>
                        </div>
                        <form action="{{ route('razorpay.store', $course->slug) }}" method="POST">
                            @csrf
                            <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{ env('RAZORPAY_KEY') }}"
                                                        data-amount="{{ $gst_price }}" data-currency="INR" data-buttontext="Checkout"
                                                        data-name="Magical Umbrella PVT. LTD" data-description="One Stop To Learn"
                                                        data-image="{{ asset('images/logo.png') }}"
                                                        data-prefill.name="{{ auth()->user()->name }}"
                                                        data-prefill.contact="{{ auth()->user()->phone_number }}"
                                                        data-prefill.email="{{ auth()->user()->email }}" data-theme.color="#0A11DB">
                            </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
