<nav>
    <a href="{{ route('welcome') }}" class="link_image">
        <img class="image_logo" src="{{ asset('images/logo.png') }}" alt="logo" />
        <span class="logo_text agency-fb">magical umbrella</span>
    </a>

    <ul>
        @guest
            <li><a href="{{ route('login') }}">Sign In/Sign Up</a></li>
            {{-- <li><a href="{{ route('register') }}">Register</a></li> --}}
        @else
            @if (auth()->user()->is_admin)
                <li><a href="{{ route('home') }}" class="mobile_hide">Admin Dashboard</a></li>
            @endif
        @endguest
        <li><a href="{{ route('about') }}">About Us</a></li>
        <li><a href="{{ route('contact') }}">Contact Us</a></li>
        <li><button class="btn_bg" onclick="openPopup()"
                style="padding: 5px 10px;border-radius: 6px;color: #fff;border:none">Syllabus</button>
        </li>
        @auth
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle p-0" role="button" data-bs-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ auth()->user()->name }}
                </a>


                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        @endauth
    </ul>
</nav>
