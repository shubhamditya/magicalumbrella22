@extends('layouts.base')

@section('title')
    Home Page
@endsection

@section('content')

    <x-alert />
    <section class="container-fluid max_width" id="hero">
        <div class="row">
            <div class="col-md-2 hero_header">
                <img src="./images/logo.png" alt="" />
            </div>
            <div class="col-md-10 hero_text">
                <h1 class="agency-fb">magical umbrella private limited</h1>
                <blockquote class="agency-fb"><i>one stop for learning</i></blockquote>
            </div>
        </div>
    </section>
    <div class="space-20"></div>
    <section class="container-fluid" id="swiper_section">
        <div class="row">
            <div class="col-md-12">
                <div class="swiper" id="mySwiper">
                    <div class="swiper-wrapper">
                        @foreach ($sliders as $slider)
                            <div class="swiper-slide">
                                <a href="#id-{{ $loop->iteration }}">
                                    <img src="{{ asset('uploads/' . $slider->image) }}" alt="" />
                                </a>
                            </div>
                        @endforeach
                        <div class="swiper-slide">
                            <div class="default_slider">
                                <h2>get syllabus</h2>
                                <button class="bg_btn" onclick="openPopup()">download</button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="space-20"></div>
    {{-- <section class="container-fluid" id="imageText">
            @foreach ($banners as $banner)
                <div class="row mt-3 {{ $loop->even ? 'flipped' : '' }}" id="id-{{ $loop->iteration }}">
                    <div class="col-md-6 image">
                        <img class="shadow" src="{{ asset('uploads/' . $banner->image) }}" alt="" />
                    </div>
                    <div class="col-md-6 text">
                        <div class="title">
                            <h5>{{ $banner->title }}</h5>
                            <p>
                                {{ $banner->description }}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </section> --}}
    <section class="container-fluid max_width" id="courses">
        <div class="row m-0">
            @foreach ($courses as $course)
                {{-- <div class="course_title">
                <h2></h2>
            </div> --}}
                <div class="col-md-4 col-sm-6 mt-4">
                    <div class="card card-manage">
                        <a href="{{ route('course.details', $course->slug) }}">
                            <img src="{{ asset('storage/' . $course->thumbnail) }}" class="card-img-top"
                                alt="{{ $course->title }}" height="200">
                        </a>
                        <div class="card-body">
                            <a href="{{ route('course.details', $course->slug) }}">
                                <h5 class="card-title">{{ $course->title }}</h5>
                            </a>
                            <p class="card-text">{{ $course->excerpt }}</p>
                            <h6>₹{{ $course->price / 100 }} <span>(₹{{ $course->discount_price / 100 }})</span></h6>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <div class="space-20"></div>
    <section class="container-fluid max_width" id="imageText">
        <div class="row flipped m-1 p-3 shadow-lg rounded">
            <div
                class="
              col-md-6
              image
              d-flex
              justify-content-center
              align-items-center
            ">
                <img class="form_logo mobile-hide" src="{{ asset('images/contact.jpg') }}" alt="" />
            </div>
            <div class="col-md-6 card shadow card_container">
                @livewire('contact-form')
            </div>
        </div>
    </section>
    <div class="space-20"></div>
    <section class="container-fluid max_width" id="iso_number">
        <div class="row">
            <div class="col-md-1">
                <img src="{{ asset('images/iso.jpg') }}" alt="">
            </div>
            <div class="col-md-4 cin">
                <span>cin no: u80902mh2021ptc367865</span>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('app.js') }}"></script>
@endsection
