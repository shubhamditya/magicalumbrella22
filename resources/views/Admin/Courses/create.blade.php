@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row mb-2">
            <div class="col-md-10">
                <div style="display: flex;justify-content: flex-end">
                    <a href="{{ route('course.index') }}" class="btn btn-primary">
                        Back
                    </a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Add Category') }}</div>
                    <form class="card-body" action="{{ route('course.store') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="course_title" class="form-label">Enter Course Title:</label>
                            <input type="text" class="form-control" id="course_title" name="title">
                            @error('title')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_id" class="form-label">Enter Category:</label>
                            <select class="form-control" id="category_id" name="category_id" id="">
                                <option value="">Select</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="excerpt" class="form-label">Enter Excerpt:</label>
                            <input type="text" class="form-control" id="excerpt" name="excerpt">
                            @error('excerpt')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_desc" class="form-label">Description</label>
                            <textarea class="form-control" id="category_desc" rows="3" name="description"></textarea>
                            @error('description')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_price" class="form-label">Enter Price:</label>
                            <input type="text" class="form-control" id="category_price" name="price">
                            @error('price')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_discount_price" class="form-label">Enter Discount Price:</label>
                            <input type="text" class="form-control" id="category_discount_price" name="discount_price">
                            @error('discount_price')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="batch_start_at" class="form-label">Batch Start At:</label>
                            <input type="date" class="form-control" id="batch_start_at" name="batch_start_at">
                            @error('batch_start_at')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="thumbnail" class="form-label">Enter Thumbnail:</label>
                            <input type="file" class="form-control" id="thumbnail" name="thumbnail">
                            @error('thumbnail')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="demo_video" class="form-label">Choose Video:</label>
                            <input type="file" class="form-control" id="demo_video" name="demo_video">
                            @error('demo_video')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
