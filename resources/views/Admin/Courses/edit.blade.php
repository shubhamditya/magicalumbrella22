@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row mb-2">
            <div class="col-md-10">
                <div style="display: flex;justify-content: flex-end">
                    <a href="{{ route('course.index') }}" class="btn btn-primary">
                        Back
                    </a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Edit Category') }}</div>
                    <form class="card-body" action="{{ route('course.update', $course->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="mb-3">
                            <label for="course_title" class="form-label">Enter Course Title:</label>
                            <input type="text" class="form-control" id="course_title" name="title"
                                value="{{ $course->title }}">
                            @error('title')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_id" class="form-label">Enter Category:</label>
                            <select class="form-control" id="category_id" name="category_id" id="">
                                <option value="" disabled>Select</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @error('excerpt')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="excerpt" class="form-label">Enter Excerpt:</label>
                            <input type="text" class="form-control" id="excerpt" name="excerpt"
                                value="{{ $course->excerpt }}">
                            @error('excerpt')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_desc" class="form-label">Description</label>
                            <textarea class="form-control" id="category_desc" rows="3"
                                name="description">{{ $course->description }}</textarea>
                        </div>
                        <div class="mb-3">
                            <label for="category_price" class="form-label">Enter Price:</label>
                            <input type="text" class="form-control" id="category_price" name="price"
                                value="{{ $course->price / 100 }}">
                            @error('price')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="category_discount_price" class="form-label">Enter Discount Price:</label>
                            <input type="text" class="form-control" id="category_discount_price" name="discount_price"
                                value="{{ $course->discount_price / 100 }}">
                            @error('discount_price')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="batch_start_at" class="form-label">Batch Start At:
                                {{ $course->batch_start_at->format('d M Y') }}(current date value)</label>
                            <input type="date" class="form-control" id="batch_start_at" name="batch_start_at"
                                value="{{ $course->batch_start_at }}">
                            @error('batch_start_at')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="thumbnail" class="form-label">Enter Thumbnail:</label>
                            <input type="file" class="form-control" id="thumbnail" name="thumbnail">
                            @error('thumbnail')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="demo_video" class="form-label">Choose Video:</label>
                            <input type="file" class="form-control" id="demo_video" name="demo_video">
                            @error('name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Add Services') }}</div>
                    <form class="card-body" action="{{ route('course.service') }}" method="post">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                        <div class="mb-3">
                            <label for="services" class="form-label">Enter Service:</label>
                            <input type="text" class="form-control" id="services" name="services">
                            @error('services')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row mt-4 justify-content-center">
            <div class="col-md-8">
                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Service</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($course->services as $service)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <th>{{ $service->services }}</th>
                                <td class="d-flex justify-content-center">
                                    <form action="{{ route('service.destroy', $service->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
