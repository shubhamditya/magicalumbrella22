@extends('layouts.base')

@section('title')
    Contact Page
@endsection

@section('content')

    <x-alert />
    <section class="container-fluid max_width" id="hero">
        <div class="row">
            <div class="col-md-12 heading_text ">
                <h1> About Us</h1>
            </div>
        </div>
    </section>
    <div class="space-20"></div>
    <section class="container-fluid" id="swiper_section">
        <div class="row">
            <div class="col-md-12">
                <div class="swiper slider_about_height" id="mySwiper">
                    <div class="swiper-wrapper">
                        @foreach ($about_sliders as $slider)
                            <div class="swiper-slide">
                                <a href="#id-{{ $loop->iteration }}">
                                    <img src="{{ asset('uploads/about/' . $slider->image) }}" alt="" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="space-20"></div>
    <section class="container-fluid max_width" id="imageText">
        @foreach ($about_banners as $banner)
            <div class="row mt-3 {{ $loop->even ? 'flipped' : '' }}" id="id-{{ $loop->iteration }}">
                <div class="col-md-7 image">
                    <img class="height-400px" class="shadow"
                        src="{{ asset('uploads/about/' . $banner->image) }}" alt="" />
                </div>
                {{-- <div class="col-md-5 text">
                        {{-- <div class="title">
                            <h3>{{ $banner->title }}</h3>
                            <p>{{ $banner->description }}</p>
                        </div> --}}
            </div>
            </div>
        @endforeach
    </section>
    <div class="space-20"></div>
    <section class="container-fluid max_width" id="gmap">
        <div class="row">
            <div class="col-md-12">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d234.37003328852362!2d79.29866937055223!3d19.96984397210277!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd2d5f71c8c39b9%3A0xd8720fa3b0287144!2sMagical%20umbrella%20pvt%20ltd.!5e0!3m2!1sen!2sin!4v1640419825105!5m2!1sen!2sin"
                    allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </section>
@endsection


@section('scripts')
    <script>
        document.getElementById('year').innerHTML = new Date().getFullYear();

        function openPopup() {
            document.getElementById('modified_popup').classList.toggle("popup_active");
        }

        new Swiper("#mySwiper", {
            spaceBetween: 30,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true
            },
            loop: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
        });
    </script>
@endsection
