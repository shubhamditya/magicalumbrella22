<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}" />
    @livewireStyles
    <title>@yield('title') | Magical Umbrella</title>
</head>

<body>
    <x-nav-bar />
    <x-popup />
    <main>
        <x-social />
        <div class="space-20 mobile-hide"></div>
        <div class="space-20 mobile-hide"></div>
        @yield('content')
    </main>
    <footer>
        <p>&copy; Copyright <span id="year"></span> Reserved Magical Umbrella Pvt. Ltd</p>
    </footer>
    @livewireScripts
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="//unpkg.com/alpinejs" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    @yield('scripts')
</body>

</html>
