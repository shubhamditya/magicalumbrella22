<?php

namespace App\Http\Controllers;

use App\Models\SliderImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = SliderImage::all();
        return view('Admin.slider.index', compact('sliders'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required',
        ]);

        $imageName = time() . $request->image->getClientOriginalName();

        $request->image->move(public_path() . '/uploads/', $imageName);

        SliderImage::create([
            'image' => $imageName
        ]);

        return back()->with('status', 'Image Uploaded Successfully');
    }

    public function destroy($id)
    {
        $slider = SliderImage::find($id);

        File::delete(public_path() . '/uploads/' . $slider->image);

        $slider->delete();

        return back()->with('status', 'Image Deleted Successfully');
    }
}
