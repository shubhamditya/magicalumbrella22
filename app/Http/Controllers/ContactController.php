<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ContactController extends Controller
{
    public function contact(Request $request)
    {

        $request->validate([
            'name' => 'required|min:6',
            'email' => 'required|email|unique:contacts,email',
            'phone_number' => 'required|digits:10|unique:contacts,phone_number',
        ]);

        Contact::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
        ]);

        $filePath = public_path("/pdf/brochure.pdf");

        return response()->download($filePath);
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);

        $contact->delete();

        return back()->with('status', 'Contact Deleted Successfully');
    }
}
