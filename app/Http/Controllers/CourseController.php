<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Course;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::all();

        return view('Admin.Courses.index', compact('courses'));
    }

    public function courseDetail(Course $course)
    {

        $course->load('services');

        $batch_bool = $course->batch_start_at >= today();

        $user_brought = false;

        if (auth()->user()) {
            foreach (auth()->user()->courseAdmissions as $courseA) {
                if ($course->id == $courseA->course_id) {
                    $user_brought = $courseA->user_id == auth()->id();
                }
            }
        }

        return view('course-details', compact('course', 'user_brought', 'batch_bool'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('Admin.Courses.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'category_id' => ['required'],
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5125',
            'demo_video' => ['required', 'mimes:mp4'],
            'excerpt' => ['required'],
            'description' => ['required'],
            'price' => ['required'],
            'discount_price' => ['required'],
            'batch_start_at' => ['required', 'after:today'],
        ]);

        $imageName = time() . '.' . $request->thumbnail->extension();
        $videoName = time() . '.' . $request->demo_video->extension();

        $imagePath = $request->thumbnail->storeAs('course_thumbnails', $imageName, 'public');
        $VideoPath = $request->demo_video->storeAs('course_demo_video', $videoName, 'public');

        Course::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'category_id' => $request->category_id,
            'excerpt' => $request->excerpt,
            'description' => $request->description,
            'price' => $request->price,
            'discount_price' => $request->discount_price,
            'batch_start_at' => $request->batch_start_at,
            'thumbnail' => $imagePath,
            'demo_video' => $VideoPath,
        ]);

        return back()->with('status', 'Course Added');
    }

    public function edit(Course $course)
    {
        $categories = Category::all();

        return view('Admin.Courses.edit', compact('course', 'categories'));
    }

    public function update(Request $request, Course $course)
    {
        $request->validate([
            'title' => ['required'],
            'category_id' => ['required'],
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5125',
            'demo_video' => ['mimes:mp4'],
            'excerpt' => ['required'],
            'description' => ['required'],
            'price' => ['required'],
            'discount_price' => ['required'],
            'batch_start_at' => ['required', 'after:today'],
        ]);

        $imagePath = $course->thumbnail;
        $VideoPath = $course->demo_video;

        if (request()->has('thumbnail')) {
            Storage::disk('public')->delete($imagePath);
            $imageName = time() . '.' . $request->thumbnail->extension();
            $imagePath = $request->thumbnail->storeAs('course_thumbnails', $imageName, 'public');
        }

        if (request()->has('demo_video')) {
            Storage::disk('public')->delete($VideoPath);
            $videoName = time() . '.' . $request->demo_video->extension();
            $VideoPath = $request->demo_video->storeAs('course_demo_video', $videoName, 'public');
        }

        $course->update([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'category_id' => $request->category_id,
            'excerpt' => $request->excerpt,
            'description' => $request->description,
            'price' => $request->price,
            'discount_price' => $request->discount_price,
            'batch_start_at' => $request->batch_start_at,
            'thumbnail' => $imagePath,
            'demo_video' => $VideoPath,
        ]);

        return back()->with('status', 'Course Updated');
    }

    public function destroy(Course $course)
    {
        Storage::disk('public')->delete($course->thumbnail, $course->demo_video);

        $course->delete();

        return back()->with('status', 'Course Deleted');
    }

    public function serviceStore(Request $request)
    {
        Service::create([
            'course_id' => $request->course_id,
            'services' => $request->services
        ]);

        return back()->with('status', 'Services Added');
    }

    public function serviceDelete(Service $service)
    {
        $service->delete();

        return back()->with('status', 'Services Deleted');
    }
}
